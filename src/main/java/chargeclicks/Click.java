package chargeclicks;

public class Click {
    private int idClick;
    private int idUser;
    private long dateClickMilliseconds;
    private String type;

    public Click(int idClick, int idUser, long dateClickMilliseconds, String type) {
        this.idClick = idClick;
        this.idUser = idUser;
        this.dateClickMilliseconds = dateClickMilliseconds;

        this.type = type;
    }

    public String getType() {
        return type;
    }

    public int getIdUser() {
        return idUser;
    }

    public long getDateClickMilliseconds() {
        return dateClickMilliseconds;
    }

}
