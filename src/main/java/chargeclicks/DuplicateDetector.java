package chargeclicks;

import java.util.ArrayList;

public class DuplicateDetector {

    public static boolean isDuplicated(ArrayList<Click> storedClicks, Click currentClick) {

        for (Click click : storedClicks) {
            {
                if (click.getIdUser() == currentClick.getIdUser()) {
                    long difDates = currentClick.getDateClickMilliseconds() - click.getDateClickMilliseconds();

                    if ((difDates < 15000) && (difDates > 0)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
