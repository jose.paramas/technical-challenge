package chargeclicks;

import java.util.ArrayList;

public class CampaignActions {

    public static String activateCampaign(int idCampaignToActivate, ArrayList<Campaign> storedCampaigns) {
        for (Campaign campaign : storedCampaigns) {
            if (idCampaignToActivate == campaign.getIdCampaign()) {

                if (isFinished(campaign)) {
                    return "finished";
                }

                return "activated";
            }
        }
        return null;
    }

    public static String deactivateCampaign(int idCampaignToDeactivate, ArrayList<Campaign> storedCampaigns) {
        for (Campaign campaign : storedCampaigns) {
            if (idCampaignToDeactivate == campaign.getIdCampaign()) {

                if (isFinished(campaign)) {
                    return "finished";
                }
                return "deactivated";
            }
        }
        return null;
    }

    public static String finishedCampaign(int idCampaignToFinished, ArrayList<Campaign> storedCampaigns) {

        for (Campaign campaign : storedCampaigns) {
            if (idCampaignToFinished == campaign.getIdCampaign()) {
                return "finished";
            }
        }
        return null;
    }

    private static boolean isFinished(Campaign campaign) {
        return campaign.getStatus().equals("finished");
    }

}
