package chargeclicks;

public class Campaign {

    private final int idCampaign;
    private String status;
    private float budget;
    private String idCountry;

    public Campaign(int idCampaign, String status, float budget) {

        this.idCampaign = idCampaign;
        this.status = status;
        this.budget = budget;
    }

    public Campaign(int idCampaign, String status, int budget, String idCountry) {

        this.idCampaign = idCampaign;
        this.status = status;
        this.budget = budget;
        this.idCountry = idCountry;
    }

    public float getBudget() {
        return budget;
    }

    public void setBudget(float budget) {
        this.budget = budget;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdCampaign() {
        return idCampaign;
    }

    public String getIdCountry() {
        return idCountry;
    }
}
