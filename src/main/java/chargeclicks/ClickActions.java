package chargeclicks;

import java.util.ArrayList;

import static chargeclicks.DuplicateDetector.isDuplicated;

public class ClickActions {

    private DemoCountryList demoCountryList;

    public ClickActions(DemoCountryList demoCountryList) {

        this.demoCountryList = demoCountryList;
    }

    public  void chargeClicks(ArrayList<Click> storedClicks, Campaign campaign) {

        if (demoCountryList.isDemo(campaign.getIdCountry())){
            return;
        }

        for (Click click : storedClicks) {

            if (isDeactivated(campaign) || isFinished(campaign)) {
                return;
            }

            if (isDuplicated(storedClicks, click)) {
                return;
            }

            if (isActivated(campaign) && hasBudget(campaign)) {
                if (isPremium(click.getType())) {
                    campaign.setBudget(campaign.getBudget() - 0.05f);
                }
                if (isNormal(click.getType())) {
                    campaign.setBudget(campaign.getBudget() - 0.01f);
                }
            }

            if (!hasBudget(campaign)) {
                campaign.setStatus("finished");
            }
        }
    }

    private boolean isNormal(String type) {
        return type.equals("normal");
    }

    private boolean isPremium(String type) {
        return type.equals("premium");
    }

    private boolean hasBudget(Campaign campaign) {
        return campaign.getBudget() > 0;
    }

    private boolean isActivated(Campaign campaign) {
        return campaign.getStatus().equals("activated");
    }

    private boolean isFinished(Campaign campaign) {
        return campaign.getStatus().equals("finished");
    }

    private boolean isDeactivated(Campaign campaign) {
        return campaign.getStatus().equals("deactivated");
    }
}
