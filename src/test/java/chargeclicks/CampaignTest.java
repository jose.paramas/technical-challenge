package chargeclicks;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CampaignTest {


    @Test
    public void campaign_status_must_be_activated(){
        int idCampaign = 123;
        String status = "activated";
        float budget = 1000;

        Campaign campaign = new Campaign(idCampaign,status,budget);

        assertEquals(campaign.getStatus(), "activated");
    }

    @Test
    public void campaign_status_must_be_deactivated(){
        int idCampaign = 123;
        String status = "deactivated";
        float budget = 1000;

        Campaign campaign = new Campaign(idCampaign,status,budget);

        assertEquals(campaign.getStatus(), "deactivated");
    }

    @Test
    public void campaign_status_must_be_finished(){
        int idCampaign = 123;
        String status = "finished";
        float budget = 1000;

        Campaign campaign = new Campaign(idCampaign,status,budget);

        assertEquals(campaign.getStatus(), "finished");
    }
}
