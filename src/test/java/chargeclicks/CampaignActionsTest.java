package chargeclicks;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class CampaignActionsTest {

    ArrayList<Campaign> campaigns = new ArrayList<>();

    @Before
    public void setup(){
        campaigns.add(new Campaign(123,"activated",1000));
        campaigns.add(new Campaign(456,"deactivated",500));
        campaigns.add(new Campaign(789,"finished",0));
    }

    @Test
    public void a_deactivated_campaign_must_be_activated(){
        int idCampaignToActivate = 456;

        String statusAfterActivated = CampaignActions.activateCampaign(idCampaignToActivate, campaigns);

        assertEquals(statusAfterActivated, "activated");
    }

    @Test
    public void a_activated_campaign_must_be_deactivated(){
        int idCampaignToDeactivate = 123;

        String statusAfterDeactivate = CampaignActions.deactivateCampaign(idCampaignToDeactivate, campaigns);

        assertEquals(statusAfterDeactivate, "deactivated");

    }

    @Test
    public void any_campaign_status_must_be_finished(){
        int idCampaignActivated = 123;

        String statusAfterFinished = CampaignActions.finishedCampaign(idCampaignActivated, campaigns);

        assertEquals(statusAfterFinished, "finished");
    }

    @Test
    public void a_finished_campaign_must_not_be_activated(){
        int idCampaignFinished = 789;

        String statusAfterActivated = CampaignActions.activateCampaign(idCampaignFinished, campaigns);

        assertEquals(statusAfterActivated, "finished");
    }

    @Test
    public void a_finished_campaign_must_not_be_deactivated(){
        int idCampaignFinished = 789;

        String statusAfterDeactivated = CampaignActions.deactivateCampaign(idCampaignFinished, campaigns);

        assertEquals(statusAfterDeactivated, "finished");
    }
}
