package chargeclicks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

public class ClickActionsTest {

    public static final int ANYBUDGET = 0;
    ArrayList<Click> clicks = new ArrayList<>();
    ClickActions clickActions;
    DemoCountryList fakeDemoCountryList;

    @Before
    public void setup(){
        fakeDemoCountryList = Mockito.mock(DemoCountryList.class);
        clickActions = new ClickActions(fakeDemoCountryList);
    }

    @Test
    public void one_click_decrease_budget_from_an_active_campaign_premium(){
       Campaign campaign = new Campaign(123,"activated",1000);
       clicks.add(new Click(1122,3333, new Date().getTime(), "premium"));

        clickActions.chargeClicks(clicks, campaign);

        assertEquals(999.95f, campaign.getBudget(), 0.0);
    }

    @Test
    public void one_click_decrease_budget_from_an_activated_campaign_normal(){
        clicks.add(new Click(1111,2222, new Date().getTime(), "normal"));
        Campaign campaign = new Campaign(123,"activated",1000);

        clickActions.chargeClicks(clicks, campaign);

        assertEquals(999.99f, campaign.getBudget(), 0.0);
    }

    @Test
    public void one_click_not_decrease_budget_from_an_deactivated_campaign(){
        clicks.add(new Click(1111,2222, new Date().getTime(), "normal"));
        Campaign campaign = new Campaign(123,"deactivated",1000);

        clickActions.chargeClicks(clicks, campaign);

        assertEquals(1000, campaign.getBudget(), 0.0);
    }

    @Test
    public void one_click_not_decrease_budget_from_an_finished_campaign(){
        clicks.add(new Click(1111,2222, new Date().getTime(), "normal"));
        Campaign campaign = new Campaign(123,"finished",1000);

        clickActions.chargeClicks(clicks, campaign);

        assertEquals(1000, campaign.getBudget(), 0.0);
    }


    @Test
    public void one_click_decrease_budget_from_an_active_campaign_without_budget_should_finished(){
        clicks.add(new Click(1122,3333, new Date().getTime(), "premium"));
        Campaign campaign = new Campaign(123,"activated",0.05f);

        clickActions.chargeClicks(clicks, campaign);

        assertEquals(campaign.getStatus(), "finished");
    }

    @Test
    public void two_click_decrease_budget_from_an_active_campaign_premium_and_normal(){
        Campaign campaign = new Campaign(123,"activated",1000);
        clicks.add(new Click(1111,3333, new Date().getTime(), "premium"));
        clicks.add(new Click(1122,4444, new Date().getTime(), "normal"));

        clickActions.chargeClicks(clicks, campaign);

        assertEquals(999.94f, campaign.getBudget(), 0.0);
    }


    @Test
    public void two_click_decrease_budget_from_an_active_campaign_premium_and_normal_and_detected_duplicated_other_click(){
        final Date date = Mockito.mock(Date.class);
        Mockito.when(date.getTime()).thenReturn(1640079668000L);

        Campaign campaign = new Campaign(123,"activated",1000);
        clicks.add(new Click(1111,3333, date.getTime(), "premium"));
        clicks.add(new Click(1122,4444, date.getTime(), "normal"));

        Mockito.when(date.getTime()).thenReturn(1640079671000L);
        clicks.add(new Click(3333,3333, date.getTime(), "premium"));


        clickActions.chargeClicks(clicks, campaign);

        assertEquals(999.94f, campaign.getBudget(), 0.0);
    }

    @Test
    public void when_a_click_belongs_to_a_demo_campaign_should_not_be_charge(){
        Campaign campaign = new Campaign(123, "activated", ANYBUDGET, "NW");
        clicks.add(new Click(1111,3333, new Date().getTime(), "premium"));

        Mockito.when(fakeDemoCountryList.isDemo(campaign.getIdCountry())).thenReturn(true);
        clickActions.chargeClicks(clicks, campaign);

        assertEquals(ANYBUDGET, campaign.getBudget(), 0.0);
        assertEquals("activated", campaign.getStatus());

    }

}
