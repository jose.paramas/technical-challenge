package chargeclicks;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;

import static chargeclicks.DuplicateDetector.isDuplicated;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DuplicateDetectorTest {

    ArrayList<Click> clicks = new ArrayList<>();

    @Test
    public void is_duplicated_when_two_click_with_three_seconds_of_difference_and_same_idUser(){
        final Date date = Mockito.mock(Date.class);
        Mockito.when(date.getTime()).thenReturn(1640079668000L);

        clicks.add(new Click(1111,3333, date.getTime(), "premium"));

        Mockito.when(date.getTime()).thenReturn(1640079671000L);
        boolean isDuplicated =  isDuplicated(clicks, new Click(1111,3333, date.getTime() , "premium"));

        assertTrue(isDuplicated);
    }

    @Test
    public void is_not_duplicated_when_two_click_with_sixteen_seconds_of_difference_and_same_idUser(){
        final Date date = Mockito.mock(Date.class);
        Mockito.when(date.getTime()).thenReturn(1640079668000L);

        clicks.add(new Click(1111,3333, date.getTime(), "premium"));

        Mockito.when(date.getTime()).thenReturn(1640079684000L);
        boolean isDuplicated =  isDuplicated(clicks, new Click(1111,3333, date.getTime() , "premium"));

        assertFalse(isDuplicated);
    }

    @Test
    public void is_not_duplicated_when_two_click_with_different_idUser(){
        final Date date = Mockito.mock(Date.class);
        Mockito.when(date.getTime()).thenReturn(1640079668000L);

        clicks.add(new Click(1111,3333, date.getTime(), "premium"));

        boolean isDuplicated =  isDuplicated(clicks, new Click(1111,4444, date.getTime() , "premium"));

        assertFalse(isDuplicated);
    }


}
