# Technical Challenge

## Objective
Our product owner wants to build a new system to charge for the clicks that we get from the website. We want
to be able to use it in different systems (a web site, an API, a desktop application) but for now we don't care
about where is running.

### Dependencies:
- openjdk-16

### Additional Information

It has been decided to load the data in memory for both the campaigns and the clicks.
